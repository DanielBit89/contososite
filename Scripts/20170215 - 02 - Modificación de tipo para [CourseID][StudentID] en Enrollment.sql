﻿CREATE TABLE [dbo].[Enrollment] (
    [EnrollmentID] UNIQUEIDENTIFIER            IDENTITY (1, 1) NOT NULL,
    [Grade]        DECIMAL (3, 2) NULL,
    [CourseID]     UNIQUEIDENTIFIER            NOT NULL,
    [StudentID]    UNIQUEIDENTIFIER            NOT NULL,
    PRIMARY KEY CLUSTERED ([EnrollmentID] ASC),
    CONSTRAINT [FK_dbo.Enrollment_dbo.Course_CourseID] FOREIGN KEY ([CourseID]) REFERENCES [dbo].[Course] ([CourseID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Enrollment_dbo.Student_StudentID] FOREIGN KEY ([StudentID]) REFERENCES [dbo].[Student] ([StudentID]) ON DELETE CASCADE
);

